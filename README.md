# PhoneLink

PhoneLink connects Asterisk Manager with the secure provisioning system SRAPS.


## Installation

PhoneLink can run within the system where Asterisk is running. A connection over the Internet is also possible, this however increases 
security risks.

### Standalone with Python3

The script requestes the following pip libraries:

* pyami-asterisk
* pyyaml
* requests
* requests_hawk

### Docker

Dockerfile

    FROM python:3-alpine

    WORKDIR /app
    COPY requirements.txt ./
    RUN pip install --no-cache-dir -r requirements.txt
    COPY ./app /app
    ENTRYPOINT [ "python3", "./sraps.py" ]

The container can be built using the following command

    docker build -t PhoneLink .

To start the image

    docker run -it -d  -v ~/snom_config.yml:/app/config.yml --name PhoneLink PhoneLink

### SRAPS Registration

    https://sraps.snom.com


## Configuration

    sraps:
        api_key: <SRAPS API Key>
        api_secret: <SRAPS API Secret>
        org_id: <Organisational ID>

    ami:
        host: <Asterisk AMI Host>
        port: <Asterisk AMI Host>
        user: <Username>
        pass: <Password>

    pgsql:
        host: <PostgeSQL Host>
        port: 1212
        user: <PostgeSQL User>
        pass: <PostgeSQL Password>
        database: <PostgeSQL Database>
        device_table: <Tables of phones>
        button_table: <Tables of the button configuration>

    logLevel: 3
    refresh: 600
    backend: <ami or pgsql>
    syslog: <IP of Syslog server>

    override:
        # registrarHost: <This overrides the settings for Transport>
        # registrarPort: 
        # userConfigPerm: R
        # More to the Permission Flags -> https://service.snom.com/display/wiki/Permission+Flags

    profiles:
      my_profile1:
        language: Nederlands

      my_profile2:
        web_language: Deutsch
        web_language: Deutsch
        syslog_server: 192.168.91.254
        log_level: '9'

    phone_common:
        MSeries:
            language: English
            web_language: English
            timezone: GER+1
            tone_scheme: GER
            http_pass: MyS3cr3t!
            http_user: admin

        DSeries:
            language: English
            web_language: English
            timezone: GER+1
            tone_scheme: GER
            http_pass: MyS3cr3t!
            http_user: admin
            admin_mode_password: MyS3cr3t!

            update_policy: auto_update
            auto_reboot_on_setting_change: 'on'
            codec_priority_list: 
                value: pcmu,pcma,telephone-event
                idx: 1


This configuration file contains several parts. This is a YAML file, the settings are *case sensitive*. An example (with the name config.yam.sample) is added to the
app directory.

The sraps settings
------------------

Containing the api_key, api_secret and org_id; These are available after a successful registration in SNOM's SRAPS Portal.

The ami settings
------------------

Host and port, as well the user and pass are mandatory. SRAPS Connect needs to talk to the Asterisk manager to obtain the configuration, as well to receive notifications.
You can find a documentation in the Asterisk manual; http://www.asteriskdocs.org/en/3rd_Edition/asterisk-book-html-chunk/AMI-configuration.html
*TLS is not implemented currently*

The basic configuration

    [general]
    enabled = yes
    port = 5038
    bindaddr = 0.0.0.0

    webenabled = no

    [admin]
    secret = admin
    read = system,call,log,verbose,command,agent,config,read,write,originate
    write = system,call,log,verbose,command,agent,config,read,write,originate

SRAPS Connect needs to be able to read pjsip.conf and must be able to issue commands, as well receive Events.
Currently only Module Reload (pjsip.so) is monitored. This triggers a refresh of the internal configuration and the configuration in SRAPS Portal.

The logLevel settings
---------------------
The following loglevels are supported:
    
    debug2:   5 -> extrem detailed; might be required for understanding SRAPS communication.
    debug:    4 -> deatailed, to see what happens under the hood
    info:     3 -> informations
    warn:     2 -> showing only warnings and errors
    error:    1 -> only errors
    disabled: 0 -> switched off

if this is not provided, only error messages are displayed.
The scripts sends all logs messages to the stderr; currently no log file, and no syslog are supported

The userConfigPerm settings
---------------------------

Snom D-Series, our Desktop Phones series supports different access levels to the provisioned configurations. 
For more detailes please refer to the SNOM Service Hub; https://service.snom.com/display/wiki/Permission+Flags


The override settings
---------------------

SRAPS Connect takes some settings dinamically from pjsip configuration, such the registration address where Snom Phones will be registered.
These are the __asteriskAddr__ and __asteriskPort__

This resolution of this address is done on the followin way

1) Each endpoint has a transport, where the __bind__ is can to be configured. 
2) If bind has a port, then this port number will be taken, else it defaults to 5060
3) if __external_signaling_address__ and __external_signaling_port__ are provided within the transport configuration, these addresses are taken
4) if __asteriskAddr__ is defined, this will override the IP Address, or host part while __asteriskPort__ the port

Currently only UDP is supported if IP Address is provided. If hostname the host resolution within the phone can also resolve TCP or TLS address.

The profiles settings
-------------------------
The pjsip phoneprov module (type=phoneprov) has two required settings; MAC (which is the MAC Address of the phone) and the PROFILE.
This profile contains phone specific settings and inherits the settings from phone_common.


The phone_common settings
-------------------------

These settings are true for every phone and will be applied for every phone.
These settings are documented on the Snom Service Hub; https://service.snom.com/display/wiki/D-Series+Settings

Some settings of the phone are global settings, some others requires a defined index number. For instance Snom D-Series Phones support multiple outgoing
SIP registrations, where the index needs to be provided in the configuration as well. 
The script defaults to 1, if no index is provided, but the setting requires an index.

Examples:

    web_language, see https://service.snom.com/display/wiki/web_language


__web\_language__ is a global setting and it does not support indexing, therefore the following settings is valid:


    web_language: Deutsch

But, as every settings can be restricted, the following configuration is also valid:

    web_language:
        value: Deutsch
        perm: R

In this case the settings will be set to german and this settings can only be manipulated by provisioning.

An example for indexed configuration item:

    codec_priority_list: 
        value: pcmu,pcma,telephone-event
        idx: 2
        perm: V

In this case the _codec\_priority\_list_ will be set to pcmu, pcma and telephone-event, for the second identity and this settings is volatile.
Remark: SRAPS Connect currently supports only one identity, therefore this should be one.


