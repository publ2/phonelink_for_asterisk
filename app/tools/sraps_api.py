#
# PhoneLink for Asterisk
#
# (C) Snom Technology GmbH 2000-2021
#
# BSD 3-Clause License
#
# Copyright (c) 2021, Snom Technology GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#    SRAPS API to communicate with https://sraps.snom.com
#
##############################################################################

import json
import requests

from requests_hawk import HawkAuth

from tools.logger import Logger

##############################################################################

class SRAPS:

    """
        SRAPS Class provides functions to interact with https://sraps.snom.com for device provisioning
        The connection is done over HTTPS (port 443).

        Documentation of SRAPS can be found on https://sraps.snom.com
    """

    SRAPS_API_VERSION = 'v1'
    SRAPS_BASE_URL = 'https://secure-provisioning.snom.com/api/' + SRAPS_API_VERSION + '/'

    def __init__(self, _sraps_config, **kwargs):
        """
            The __init__  function not just constructs this object but loads also some required setting
            which are stored in SRAPS and initializes the HawkAuthentication using the provided credentials.

            These required settings are:

                roots/links/settings: this object contains all settings which are currently known for SRAPS.
                    These are not just used to identitfy the UUID of the setting, which is required for the
                    setting_manager object, but also required to do some basic validation of the value provided
                    for the setting.

                roots/links/products: this object lists all known products of Snom. Each type are identified
                    by its MAC address range, which helps to identify the correct type and model of the devie
                    based on its MAC address.

                roots/links/companies: every available information for the company itself. This is requested
                    mainly to identify the child companies.

            Initalizing the loggin.
                If "log" is not provided in the kwargs dict it will be initialized with 1, which is the
                level "Error".


            @param _sraps_config: is a dict and conatins the connection information to SRAPS.

            @param kwargs: Here additional objects are passed to this library. No it is only the "log",
                which implements logging (see tools/logger.py).

            @return: None

        """

        self.sraps_config = {}
        self.sraps_config['api_key'] = _sraps_config.get('api_key', '')
        self.sraps_config['api_secret'] = _sraps_config.get('api_secret', '')
        self.sraps_config['org_id'] = _sraps_config.get('org_id', '')

        self.sraps = {}

        self.sraps['roots'] = {}
        self.sraps['settings'] = {}
        self.sraps['company_info'] = {}
        self.sraps['products'] = {}
        self.sraps['endpoints'] = {}

        self.hawk_auth = HawkAuth(
            id=self.sraps_config['api_key'],
            key=self.sraps_config['api_secret'],
            always_hash_content=False)

        self.log = kwargs.get('log', Logger(1))

        headers = {'accept': 'application/json'}

        response = requests.get(url=self.SRAPS_BASE_URL, headers=headers)
        if response.status_code != 200:
            return
        self.sraps['roots'] = response.json()

        response = requests.get(url=self.sraps['roots']['links']['settings'], headers=headers, auth=self.hawk_auth)
        if response.status_code != 200:
            return
        self.sraps['settings'] = response.json()

        response = requests.get(url=self.sraps['roots']['links']['products'], headers=headers, auth=self.hawk_auth)
        if response.status_code != 200:
            return
        self.sraps['products'] = response.json()

        response = requests.get(url=self.sraps['roots']['links']['companies']
                                + self.sraps_config['org_id'], headers=headers, auth=self.hawk_auth)
        if response.status_code != 200:
            return

        self.sraps['company_info'] = response.json()

    def get_settings_by_name(self, product=None, name='', value='', **kwargs):
        """
            Provides settings by the given name.

            Settings are received from SRAPS for each type of device and this function returns the complet
            setting object which can be added the setting_manager sraps dict.

            This function loops through the loaded /sraps/settings and matched with the provided name
            of the setting where the product type also matches. This is important to know that some settings
            are overlapping between D-Series and M-Series, which are having the same name but differene UUID
            and sometimes different values are possible.

            If the setting has an index value (according to SRAPS) then the index value is also added.
            This is here defaults to one if not presented in the value dict.

            @param product: type of the product (see get_product)

            @param name: name of the variable, for example: "setting_server"

            @param value: value for the given settings

            @param user_perm: the requested permission string
                (see: https://docs.snom.com/display/wiki/Permission+Flags)

            return with a dict of the settings

        """
        ret = {}

        _user_perm = kwargs.get('user_perm', 'RW')
        _type = kwargs.get('type', None)
        _context = kwargs.get('context', None)
        _label = kwargs.get('label', None)
        _short_label = kwargs.get('short_label', None)

        if not isinstance(value, dict):
            _t = {}
            _t['value'] = value
            _t['idx'] = 0
            value = _t

        for _set in self.sraps['settings']:
            if _set['param_name'].lower() == name.lower():
                if len([value for value in _set['product_groups'] if value in product['groups']]) >= 1:
                    ret = {
                        _set['uuid']: {
                            'value': value['value'],
                            'attrs': {
                                'perm': _user_perm
                            }
                        }
                    }
                    if value.get('type', None) is not None:
                        ret[_set['uuid']]['type'] = value['value']

                    if _set['indexed']['enabled'] is True:
                        idx = 1  # use default index 1 ( first identity )
                        if int(value['idx']) <= _set['indexed']['max_size'] and int(value['idx']) >= _set['indexed']['start']:
                            idx = value['idx']
                        ret = {
                            _set['uuid']: {
                                'idx' + str(idx): {
                                    'value': value['value'],
                                    'attrs': {
                                        'perm': _user_perm
                                    }
                                }
                            }
                        }
                        if _type is not None:
                            ret[_set['uuid']]['idx' + str(idx)]['attrs']['type'] = _type
                        if _context is not None:
                            ret[_set['uuid']]['idx' + str(idx)]['attrs']['context'] = _context
                        if _label is not None:
                            ret[_set['uuid']]['idx' + str(idx)]['attrs']['label'] = _label
                        if _short_label is not None:
                            ret[_set['uuid']]['idx' + str(idx)]['attrs']['short_label'] = _short_label

                    return _set['uuid'], ret
        return None, None
        # return _set['uuid'], ret

    def get_product(self, mac=0):
        """
            Based on the given mac address returns which product categories applies for the device.
            SRAPS knows every MAC Address ranges what Snom assigned to the devices. We use this to identify
            which product category the device belongs to and what settings are valid for the given device.

            @param mac: the mac address

            @return: dict
                    - code: the product code, for example: snomD785, this is the same as the placeholder {model}
                            in the phone
                    - groups: list of settings groups, where the settings can be found to check.
                              get_settings_by_name checks against this list if the evaluated setting
                              for the product valid is
                    - type: the type of the product, derivered from the upgrade strategy; this can be Dseries or Mseries

        """
        _r = {}
        _mac = int("0x" + mac, 16)
        for item in self.sraps['products']:
            for _range in item['mac_ranges']:
                _s = int(_range['start'], 16)
                _e = int(_range['end'], 16)
                if _s < _mac < _e:
                    _r['code'] = item['code']
                    _r['groups'] = item['groups']
                    _r['type'] = item['firmware_strategy']['strategy']
                    return _r
        return None

    def load_endpoints(self):
        """
            Retrievs a list of endpoints from SRAPS which belong to the confgured company id.

            @return: none
        """
        headers = {'accept': 'application/json'}

        response = requests.get(url=self.sraps['roots']['links']['companies']
                                + self.sraps_config['org_id']
                                + '/endpoints/',
                                headers=headers,
                                auth=self.hawk_auth)

        if response.status_code != 200:
            self.sraps['endpoints'] = {}

        self.sraps['endpoints'] = response.json()

    def get_endpoint(self, mac=''):
        """
            Fetches all stored settings for the given MAC address

            @param mac: is the requested MAC, in form of a MAC, f.e.: 000413XXXXXX

            @return a json object containing the setting_manager settings of the given MAC address

        """
        headers = {'accept': 'application/json'}

        response = requests.get(url=self.sraps['roots']['links']['companies']
                                + self.sraps_config['org_id']
                                + '/endpoints/' + mac.lower()
                                + '/explain',
                                headers=headers,
                                auth=self.hawk_auth)

        if response.status_code != 200:
            return {}

        return response.json()

    def set_endpoint(self, mac='', settings=None):
        """
            Stores or creates the given device (MAC address) in SRAPS.

            If the MAC address does not exists in SRAPS, it will be created, under the given organisation_id
            If exists, the setting_manager will be overwritten with the new settings.

            It might happen, that the MAC address exists, but not in the given organisation_id. In this case
            SRAPS will refuse to update the device. This is sinalised HTTP Status Code 401. Such exception
            is not handled here.

            param mac: the MAC Address of the phone
            param settings: the dict of the setting_manager
        """

        headers = {'accept': 'application/json'}
        data = json.dumps(settings)

        response = requests.put(url=self.sraps['roots']['links']['companies']
                                + self.sraps_config['org_id']
                                + '/endpoints/'
                                + mac.lower(), headers=headers, data=data, auth=self.hawk_auth)

        self.log.debug2("SRAPS Response: ", response.json(), " Code: ", response.status_code)
        return (response.status_code == 200) or (response.status_code == 201)
