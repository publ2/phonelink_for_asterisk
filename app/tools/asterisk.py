#
# PhoneLink for Asterisk
#
# (C) Snom Technology GmbH 2000-2021
#
# BSD 3-Clause License
#
# Copyright (c) 2021, Snom Technology GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
##############################################################################

from os import TMP_MAX
import sys
import re

from pyami_asterisk import AMIClient

from tools.logger import Logger
from tools.config import Config

##############################################################################


class Asterisk:
    """
        Asterisk functions which are using the pyami_asterisk library. These are not visiblae
        outside of this.

        This is using the previously provided logger and config objects. If they are missing a
        a local Logger will be created and log will be switched to off (0).

    """

    def __init__(self, _connection, **kwargs):
        """
            Initializing Asteriks client.

            The configuraiton is however provided through the first agrument additional setting
            are also possible.

            In kwargs we expect the "logger" and the "Config" object. These objects are used
            in this library to produce logging and manipulating the configuration. If they
            are not provided, they are initialized here with default settings.

            @param _connection: dict, which contains the connection information for the Asterisk Manager
            these are the host, port, user and pass.

            @param kwargs: Here are passed the named objects, like "log" and "config", These are initialized elsewhere
            and already containing settings.

            @return: None

        """

        self._asterisk = {}

        self._connection = _connection

        self.log = kwargs.get('log', Logger(0))
        self.config = kwargs.get('config', Config(configFileName="config.yml", log=self.log))

    def connect(self):
        """
            This will initialize the AMIClient from pyami_asterisk with the provided configuration.

            The connect method establishes the TCP connection to Asterisk and
            enters into the main loop.

            Connect will also initiate an Action: Login.
            Once Login was accepted the "FullyBooted" Event is triggered and the _asterisk_event object
            get called. This event will load the settings from Asterisk

            @param: None

            @return: None

        """

        self._asterisk = AMIClient(
            host=self._connection.get('host', '127.0.0.1'),
            port=self._connection.get('port', '5038'),
            username=self._connection.get('user', ''),
            secret=self._connection.get('pass', '')
        )

        self._asterisk.register_event(["*"], callbacks=self._asterisk_events)

        self._asterisk.connect()

    def command(self, action, callback=None):

        _callback = callback

        if _callback is None:
            _callback = self._asterisk_events

        self._asterisk.create_action(action, _callback)

    def get_config(self):
        """
            Requests configuration from Asterisk / PJSET

            With the version 18 there are different Events for AOR, Endpoint and also Transport available;
            Since Version 16 is still stable and used widly we keep these older method.

            Asterisk also provides the GetConfigJSON Action, which provids the requested configuration file
            as a JSON object. This is convinient, however the async IO Lib for Asterisk is looking for an endline
            character (\n). If that is not found, but the allocated buffer is full an exception is raised.
            This can happen with a bigger pjsip.conf, therefore the GetConfig Action is used.

            This action returns the "classical" response from AMI by listing the Categories
            and the Lines which belogs to them.

            This method has no parameter and no result. The event will call the defined callback function
            (asterisk_get_config_event) when AMI returns with the data (in LINE format).

        """

        self._asterisk.create_action({
            "Action": 'GetConfig',
            "Filename": "pjsip.conf",
            "ActionID": 'GetConfig'
        }, self._asterisk_get_config_event)

    def _asterisk_get_config_event(self, event, **kwargs):  # pylint: disable=unused-argument
        """
            Processing GetConfig Event.

            The returning dict (event) contains the configuration in Key: Value format. These are delimited
            by end-line character (\n)

            The following lines are processed:

            Key Category-XXXXXXXX, where XXXXXXXX is a number represents the name of the category, so
            event['Category-XXXXXXXX] returns the name of the given category. In pjsip.conf (or any)
            included files the categories are written between brackets, f.e.: [transport-udp].

            Key Line-XXXXXXXX-YYYYYYYY represents the configuration item. The value is a key=value pair, like
            in the pjsip.conf file (f.e.: type=transport). In the name of the key the XXXXXXXX represents the
            number of the Category while the YYYYYYYY represents the number of the line.
            These numbers are unreliable. If a Category is inserted at the top of the file the numbering after
            reloading the module will be shifted.

            The loaded configuration is traversed based on the "type" value and it makes in the internal configuration
            registry possible to fetch an element based on its path, like: /pjsip/transport/udp-transport

            Using the add method of the config object the created dict will be added as "pjsip" to the configuration.

            @param event: Contains the event related information, as well the configuration file as multiple lines

            @param kwargs: Required if the event might return additional data; GetConfig does not use this

            No Return value

        """
        self.log.debug2("Received GetConfig", event)

        if event['Response'] == "Success":

            _tmpCfg = {}

            _category = re.compile("^(Category)-([0-9]+)$")
            _cfgline = re.compile("^(Line)-([0-9]+)-([0-9]+)$")

            for _line in event:

                _c = _category.search(_line)
                _l = _cfgline.search(_line)

                if _c is not None:
                    _c_num = int(_c.group(2), base=10)

                    if event[_line] not in _tmpCfg:
                        _tmpCfg[_c_num] = {'categoryName': event[_line]}

                elif _l is not None:
                    _lineCategory = int(_l.group(2), base=10)
                    # _lineSerial = int(_l.group(3), base=10)
                    if _tmpCfg[_lineCategory] is None:
                        _tmpCfg[_lineCategory] = {}

                    # using rsplit, 1 since the settings might contain =; example setvar=location=0 (sip.conf)
                    _set, _val = event[_line].rsplit('=', 1)
                    _tmpCfg[_lineCategory].update({_set: _val})

            _finCfg = {
                'global': {},
                'transport': {},
                'aor': {},
                'auth': {},
                'phoneprovr': {},
                'endpoint': {}
            }

            for _l, _block in _tmpCfg.items():

                _category_name = _block['categoryName']
                _block.pop('categoryName')

                if 'type' in _block:
                    _type = _block['type']
                    if _type in _finCfg.keys():
                        _finCfg[_type].update({_category_name: _block})

                elif _category_name == 'global':
                    _finCfg['global'] = _block

            _phoneprov_tmp = _finCfg['phoneprovr'].copy()
            _finCfg['phoneprovr'] = []

            for _, _item in _phoneprov_tmp.items():
                if _item.get('mac', None) is None:
                    continue

                _finCfg['phoneprovr'].append(_item)

            self.log.debug2("PJSIP Configuration ", _finCfg)
            self.log.info("PJSIP Configuration loaded")

            self.config.add('/pjsip', _finCfg)
            self.config.dump()

        else:
            self.log.info("Can not load PJSIP Config")
            self.log.error("Evet: ", event)

    def _asterisk_events(self, event, **kwargs):  # pylint: disable=unused-argument
        """
            Processing Events from Asterisk; the response from commands are also reaching this function.

            This function is used to catch events which do not requires deeper logic or they are simple.
            For every other Event a specific function is created right after the function, which sends
            the request to AMI.

            This function is taken as a default Event Handler for Asterisk, if command does not get
            a callback function.

            Events in this function:
                - FullyBooted
                  This event occures right after the login and it is to be interpreted like AMI is Ready.
                  Therefore we load the configuration of PJSIP using the asterisk.get_config function.

                - Reload
                  This event is fired if a module is reloaded. Now we only support pjsip.
                  If that is occured we have to reload the configuration of PJSIP, since an extension
                  might be changed.

                - InboundRegistrationDetail
                  This Event is fired if a "PJSIPShowRegistrationsInbound" action was executed and returning to AMI.
                  We use this here to send a SIP Notify with Check-Sync event to the registered contact in Asterisk.
                  A usecase is when a parameter (for instance the SIP Password) was changed in the configuration.
                  If that happend the pjsip module was reloaded (see Event Reload) and the configuration was reloaded,
                  which triggerd an update in SRAPS. If that happens the configuration in Asterisk and SRAPS are new
                  and valid, while the password configured in phone the old one. A Check-Sync event however requests
                  the phone to load the configuration immediately.

            Responses in this function:
                Responses are generated by some AMI Actions. Just like Events here are Responses, which do not require
                complicated processing, or they are just straghtforward.

                - Logoff
                  Logoff is generated after Logoff; there is no way to answer and the only possible action is
                  to exit. If AMI does not say "Goodbye" acknoloding the exit this function still exists.

            @param event: contains the event itself

            @param kwards: added since event might have additional attributes

            @return: None

        """
        if 'Event' in event:

            self.log.debug("Asterisk Event recevied, ", event['Event'])

            if event['Event'] == "FullyBooted":
                self.log.debug("AMI ist ready")
                self.get_config()

            elif event['Event'] == "Reload":
                if event['Module'] == 'res_pjsip.so':
                    self.log.info("PjSIP was reloaded, reloading configuration")
                    self.get_config()
                else:
                    pass

            elif event['Event'] == 'InboundRegistrationDetail':
                self.log.info("Sending Check-Sync to ", event['ObjectName'])
                self._check_sync(event['ObjectName'], False)

            self.log.debug("Event processed")

        elif 'Response' in event:

            self.log.debug("Asterisk Response handler, ", event['ActionID'])

            status = event['Response']
            action = re.match(r'([A-Za-z]+)([0-9]+)?', event['ActionID'])[1]

            self.log.debug("Processing ", action)

            if action == 'Logoff':
                if status == "Goodbye":
                    self.log.info("Logout from Asterisk...")
                    sys.exit(0)
                else:
                    self.log.error("Logout was not possible, exiting anyway...", event)
                    sys.exit(1)

            else:
                self.log.debug2("No acton matched")

            self.log.debug("End processing respone")