#
# PhoneLink for Asterisk
#
# (C) Snom Technology GmbH 2000-2021
#
# BSD 3-Clause License
#
# Copyright (c) 2021, Snom Technology GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
##############################################################################

import sys


class Logger:
    """ Logger Class with predefined log levels """

    DEBUG_LEVELS = {
        "debug2": 5,
        "debug": 4,
        "info": 3,
        "warn": 2,
        "error": 1,
        "disabled": 0
    }

    def __init__(self, debug_level=2):
        self.debug_level = debug_level
        self.error_fileno = sys.stderr

    def level(self, new_level=2):
        """
            Set debug level to the provided value
        """

        self.debug_level = new_level

    def debug2(self, *log):
        """
            Logs parameers with the given level
        """

        self._log('debug2', *log)

    def debug(self, *log):
        """
            Logs parameers with the given level
        """

        self._log('debug', *log)

    def info(self, *log):
        """
            Logs parameers with the given level
        """

        self._log('info', *log)

    def warn(self, *log):
        """
            Logs parameers with the given level
        """

        self._log('warn', *log)

    def error(self, *log):
        """
            Logs parameers with the given level
        """

        self._log('error', *log)

    def _log(self, log_level, *args):
        """
            Gathers the provided argumenst and prines with a trailing new line through stderr
        """

        if self.debug_level < self.DEBUG_LEVELS[log_level]:
            return

        msg = ""
        for _l in args:
            msg += str(_l)
        self.error_fileno.write(msg + "\n")
