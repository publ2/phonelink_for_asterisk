#
# PhoneLink for Asterisk
#
# (C) Snom Technology GmbH 2000-2021
#
# BSD 3-Clause License
#
# Copyright (c) 2021, Snom Technology GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
##############################################################################

import sys
import yaml
import tempfile

from tools.logger import Logger


class Config:
    """ Provide an interface for YAML Config File """

    def __init__(self, **kwargs):
        self.config = {}
        self._notify = {}

        self.config_file_name = ''
        self.config_file_name = kwargs.get('configFileName', 'config.yml')

        self.log = kwargs.get('log', Logger(1))
        self.log.debug("Config Object initailized")

    def load(self):
        """
            Load configuration file and overwrite existing
        """

        self.config = {}
        self.config['bridge'] = {}

        try:
            with open(self.config_file_name, "tr") as cfg:
                _c = cfg.read()
                self.log.debug2("Yaml Configuration: ", _c)
                self.add('/bridge', yaml.load(_c, Loader=yaml.FullLoader))

        except: # pylint: disable=W0702
            self.log.error("Config can not be loaded ", sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2])

        self.log.debug("Loaded config ", self.config)

    def get(self, path, default_value=None):
        """
            get a configuration value provided by path;
            syntax: /object/subbject/subobject/variable
        """
        _path = path.split('/')

        try:
            if _path[0] == '':
                _path.pop(0)
        except:  # pylint: disable=W0702
            pass

        if len(_path) > 0:
            if _path[0] == '':
                return (self.config)
            else:
                return self._get_next_level(_path, self.config, default_value)

        return default_value

    def add(self, path, config_elements=None):
        """
            Create / overwrite a configuration root

            The function will create a new leaf from the root of the self.config.
            If the provided "path" contains a path, with multiple levels (such es: /config/test/data)
            the function (using _get_next_level) will go until the not most last and will
            assigned te given value.

            If a subscription exists to the root (thorugh self.subscribe) that functions
            are also called with the parameter "path" indicating which path is calling the callback and
            with the config which was just added to the given path

            @param: path is an arbitary path, where the new configuration will be added.
            For example: /bridge will create a dict in the root and /bridge/test a dict under the bridge,
            with the name dict.

            @param config_elements: defaults to None, if not provided. This will only then create the path.
            If a value is provided, this will be assigned to the created dict.

            @return: None

        """

        _path = path.split('/')

        try:
            if _path[0] == '':
                _path.pop(0)
        except:  # pylint: disable=W0702
            pass

        _last = _path.pop()

        _l = self._get_next_level(_path, self.config, None)

        if _l is None:
            self.log.error("Path does not exists ", _path)
            return

        _l[_last] = config_elements

        if path not in self._notify:
            return

        for cb in self._notify[path]:
            cb({
                'path' : path,
                'config': config_elements
            })

    def subscribe(self, path='', callback=None):
        """
            Suscription for config change

            If a function needs to be executed if a path was changed, a function can be subscribed using this method.
            This will append an entry to the self._notify and self.add will call these functions.

            @param path: The path; when the value of this is changed these functions are called.
            The path represents the last element; if the /bridge/ami/host is changed every function will be triggered
            which is subscribed for the whole path "/bridge/ami/host".

            @param path: the path to subscribe to

            @param callback: The function which needs to be executed.

            @return: None

        """

        if callback is None:
            return

        if path not in self._notify:
            self._notify[path] = []

        self._notify[path].append(callback)

    def dump(self):
        """
            Dumping the configuration

            This function will print (to the stdout) the configuration in yaml format and
            creates a file with a uniq name in /tmp and also dumps it there.

            @return: None

        """
        cfg = yaml.dump(self.config, indent=4, allow_unicode=False)
        self.log.debug2("Config Dump", cfg)

        out_file = tempfile.NamedTemporaryFile(delete=False, suffix=".yml")
        out_file.write(bytes(cfg, 'utf-8'))
        self.log.info("Dump into file ", out_file.name)
        out_file.close()

    def _get_next_level(self, path, cfg=None, default_value=None):
        """
            Recursive walk through the cfg according to the path

            This function walks through the configuration based on the path and returns with the last element
            which is found.

            The return is either the default_value, or, if succeed the value of the last item from the path.

            @param: path is an array of elements

            @param: cfg dict, the configuration item

            @param: default_value, will return if one element from the path can not be found

            @return: either the config element from cfg parameter, or the default_value
        """

        if len(path) == 0:
            return cfg

        _item = path.pop(0)

        if _item not in cfg:
            return default_value

        if len(path) > 0 and isinstance(cfg[_item], dict):
            return self._get_next_level(path, cfg[_item], default_value)

        return cfg[_item]
