#
# PhoneLink for Asterisk
#
# (C) Snom Technology GmbH 2000-2021
#
# BSD 3-Clause License
#
# Copyright (c) 2021, Snom Technology GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
##############################################################################

from os import TMP_MAX
import sys
import re

import psycopg2
from psycopg2.extras import RealDictCursor

from tools.logger import Logger
from tools.config import Config

##############################################################################
BUTTON_TYPES = (
    'none',
    'dest',
    'blf',
    'speed',
    'line',
    'none',
    'park',
    'none'
)

FKEY_MAX = 8    # 32 + ((3 * 16) * 1)

class PGsql:
    """
        PostgreSQL Database backend.

        This class reads the configuration from PosteSQL Tables(s) and creates the original structure,
        like it was used with Asterisk.

    """

    def __init__(self, _connection, **kwargs):
        """
            Initiating the pgsql backend.

            param: _connection contains the settings, where the PostgeSQL is accessible.
            param: kwargs has one used member; called "config", which is the configuration object.

        """

        self._pgsql = {}
        self._connection = _connection

        self.log = kwargs.get('log', Logger(0))
        self.config = kwargs.get('config', Config(configFileName="config.yml", log=self.log))

    def connect(self):

        try:
            self._pgsql = psycopg2.connect(
                host=self._connection.get('host', '127.0.0.1'),
                database=self._connection.get('database', 'sraps'),
                user=self._connection.get('user', 'sraps'),
                password=self._connection.get('pass', 'sraps')
            )

            cur = self._pgsql.cursor()

            cur.execute('SELECT version()')
            db_version = cur.fetchone()

            self.log.info("Connected to ", db_version)

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:
            self.log.error("Error occured, ", error)
            return None

    def command(self, action, callback=None):
        self.log.debug("No backend command supported")
        return None

    def get_config(self):
        return self._get_config()

    def _get_config(self):

        _finCfg = {
            'global': {},
            'transport': {},
            'aor': {},
            'auth': {},
            'phoneprovr': [],
            'endpoint': {},
            'buttons': {}
        }

        _idx = {}

        req = self._cursor()
        req.execute("SELECT * FROM " + self._connection.get('device_table', 'sraps_devices'))

        for _row in req.fetchall():
            row = self._make_associative(_row, [desc[0] for desc in req.description])
            _id = str(row.get('user_name', '')) + '@' + str(row.get('transport', ''))
            _mac = row.get('mac')

            _auth = 'auth_' + _id
            _transport = 'transport-' + row.get('transport', '')

            if _id is None or _mac is None:
                continue

            try:
                _idx[_mac].append(_id)
            except KeyError:
                _idx[_mac] = ['idx']
                _idx[_mac].append(_id)

            _finCfg['phoneprovr'].append({
                'endpoint': _id,
                'mac': row.get('mac', 'mac'),
                'profile': 'snom_default',
                'type': 'phoneprovr',
                'idx': _idx[_mac].index(_id)
            })

            if _id not in _finCfg['endpoint']:
                _finCfg['endpoint'][_id] = {
                    'aors': '',
                    'auth': _auth,
                    'from_domain': row.get('transport', ''),
                    'user_name': row.get('user_name', ''),
                    'transport': _transport,
                    'display_name': row.get('e_display_name', row.get('user_name', '')),
                    'type': 'endpoint'
                }

            if _auth not in _finCfg['auth']:
                _finCfg['auth'][_auth] = {
                    'auth_type': 'userpass',
                    'password': row.get('user_pname', ''),
                    'type': 'auth',
                    'username': row.get('username', ''),
                    'ldap_username': row.get('ldap_username', row.get('username')),
                    'ldap_password': row.get('ldap_password', ''),
                    'ldap_base': row.get('ldap_base', ''),
                    'realm': ''
                }

            if _transport not in _finCfg['transport']:
                _finCfg['transport'][_transport] = {
                    'bind': row.get('transport', ''),
                    'protocol': '',
                    'type': 'transport'
                }

        req.close()

        req = self._cursor()
        req.execute("SELECT * FROM " + self._connection.get('button_table', 'sraps_buttons'))

        for _row in req.fetchall():

            row = self._make_associative(_row, [desc[0] for desc in req.description])

            _id = str(row.get('user_name', '')) + '@' + str(row.get('transport', ''))
            _mac = row.get('mac')
            _type = BUTTON_TYPES[int(row.get('btyp', None))]
            _button_pos = int(row.get('bpos', 2)) - 1

            if _id == '@' or _mac is None:
                continue

            if _mac not in _finCfg['buttons']:
                _finCfg['buttons'][_mac] = {}

                for i in range(FKEY_MAX):
                    _finCfg['buttons'][_mac][i] = {
                        'type': 'none',
                        'attr': '',
                        'label': ' ',
                        'short_label': ' ',
                        'context': 1
                    }

            _finCfg['buttons'][_mac][0] = {
                'type': 'line',
                'attr': '',
                'label': row.get('user_name'),
                'short_label': row.get('user_name'),
                'context': 1
            }

            """
                If fkey type is line, we make sure that one identity is created for the sip extension
                and we set the context to the new id of the identity
            """

            if _type == 'line':
                if _mac not in _idx:
                    _idx[_mac] = ['idx']

                if _id not in _idx[_mac]:
                    _idx[_mac].append(_id)
 
                _auth = 'auth_' + _id
                _transport = 'transport-' + row.get('transport', '')
                _context = _idx[_mac].index(_id)

                _finCfg['phoneprovr'].append({
                    'endpoint': _id,
                    'mac': _mac,
                    'profile': 'snom_default',
                    'type': 'phoneprovr',
                    'idx': _idx[_mac].index(_id)
                })

                if _id not in _finCfg['auth']:
                    _finCfg['auth'][_auth] = {
                        'auth_type': 'userpass',
                        'password': row.get('user_pname', ''),
                        'type': 'auth',
                        'username': row.get('username', ''),
                        'realm': ''
                    }

                if _id not in _finCfg['endpoint']:
                    _finCfg['endpoint'][_id] = {
                        'aors': '',
                        'auth': _auth,
                        'from_domain': row.get('transport', ''),
                        'user_name': row.get('user_name', ''),
                        'transport': _transport,
                        'display_name': row.get('b_name', row.get('user_name', '')),
                        'type': 'endpoint'
                    }

                _finCfg['transport'][_transport] = {
                    'bind': row.get('transport', ''),
                    'protocol': '',
                    'type': 'transport'
                }

            try:
                _context = _idx[_mac].index(_id)
            except ValueError:
                _context = 1

            _finCfg['buttons'][_mac][_button_pos] = {
                'type': _type,
                'attr': row.get('service', ' '),
                'label': row.get('b_name'),
                'short_label': row.get('b_name'),
                'context': _context,
                'mac': _mac
            }

        self.config.add('/pjsip', _finCfg)
        # self.config.dump()
        req.close()

    def _make_associative(self, row, cols):
        _ret = {}

        for _col in row:
            _ret[cols.pop(0)] = _col

        return _ret

    def _cursor(self):
        return self._pgsql.cursor()


