#
# PhoneLink for Asterisk
#
# (C) Snom Technology GmbH 2000-2021
#
# BSD 3-Clause License
#
# Copyright (c) 2021, Snom Technology GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
##############################################################################

import sys
import time
import signal
from threading import Timer

from tools.logger import Logger
from tools.sraps_api import SRAPS
from tools.config import Config
from tools.asterisk import Asterisk
from tools.pgsql import PGsql

##############################################################################


class Main:
    """
        Main Class, holds all the needed functions to communicated with SRAPS (via SRAPS API) and with the backend.
        Now we support Asterisk through AMI and PGSql.

    """

    def __init__(self):
        """
            Init will create some vital variables for the running. One of this is the "Logger", as self.log.
            The initialisation value (1) is the loglevel, which is the INFO.

            Also the config object is created and initialised here with the filename "config.yml" and
            the content of the file will be also loaded by using the load method.

            More to read about this, please refer to the config.ym.sample

            Init also initializes the SRAPS object, which will communicate with
            Snom's Secure Redirection Server (SRAPS) by providing higher functions to interact with the REST API.

            Once SRAPS is initialized it also downloaded all the settings of Snom Phones.

        """

        self.log = Logger(4)
        self.config = Config(configFileName="config.yml", log=self.log)
        self._scheduler = {}

        self.config.load()
        self.log.level(self.config.get('/bridge/logLevel', 1))

        self.sraps = SRAPS(self.config.get('/bridge/sraps', {}), log=self.log)
        if self.sraps is None:
            self.log.info("Connection to SRAPS is not possible.")
            sys.exit(1)

        self._backend = self.config.get('/bridge/backend', None)

        if self._backend == 'ami':
            self.backend = Asterisk(self.config.get('/bridge/ami', {}), log=self.log, config=self.config)
            if self.backend is None:
                self.log.info("Can not initialise Asterisk")
                sys.exit(1)
        elif self._backend == 'pgsql':
            self.backend = PGsql(self.config.get('/bridge/pgsql', {}), log=self.log, config=self.config)

            if self.backend is None:
                self.log.info("Can not initialise PGsql")
                sys.exit(1)

        else:
            self.log.info("No backend")
            sys.exit(1)

    def start(self):
        """

            Starting up the process; for Asterisk the script establishes a connection to AMI, for PGSql it connects
            to the Postgresql Server.

            For exit and interrupt we use the QUIT, INT and TERM signals. This also covers the requirement
            of Docker, which sends a TERM upon a stop request.

            For graceful stop the Script executes an Action: Logoff on Asterisk AMI, which also terminates the connection.
            The successfull Logoff will trigger the Logoff event which calls the system exit.

        """
        signal.signal(signal.SIGHUP, self._signal_handler)
        signal.signal(signal.SIGINT, self._signal_handler)
        signal.signal(signal.SIGTERM, self._signal_handler)

        signal.signal(signal.SIGUSR1, self._signal_handler)
        signal.signal(signal.SIGUSR2, self._signal_handler)

        self.config.subscribe('/pjsip', self._sraps_update)

        _refresh = self.config.get('/bridge/refresh', 600)

        self.backend.connect()

        self.set_interval(10, self._log_keep_alive)
        self.set_interval(_refresh, self.backend.get_config)

    def _log_keep_alive(self):
        """
            Printing keepalive ins Log with a timestamps; this is useful to see if the script
            is still runnning. Normally this script has only a task if a modul is restarted.
        """
        _now = time.localtime()

        self.log.info(time.strftime('%Y-%m-%d %H:%M', _now), " - Bridge is running - Keepalive")

    def _sraps_get_auth(self, auth):
        """
            Lookup the authenication (type=auth) with the providede name in PJSIP Configuration.
            This authentication credenials are configured for the identity for the phone.
            Snom makes possible to provision user_hash, see:
            https://docs.snom.com/display/wiki/Can+I+provide+encrypted+user+passwords+via+mass+deployment

            This is not supported by the script, since the communication between PhoneLink and SRAPS
            is secured with TLS 1.2 and the telephone is always provisioned by SRAPS, which also takes care
            the validation of the MAC Address usin SHA256 certificates.

            @param auth: the name of the authentication element within pjsip.conf

            @return: dict
                    - username: the username configured for the identity
                    - password: the given password
                    - realm: real, where the credentials are valid

        """

        self.log.debug("Lookup Authentication for " + auth)

        _auth = self.config.get('/pjsip/auth/' + auth, {})

        _ret = {
            "username": _auth['username'],
            "password": _auth['password'],
            "realm": _auth['realm']
        }

        if _auth.get('ldap_username', None) is not None:
            _ret['ldap_password'] = _auth.get('ldap_password', ' ')
            _ret['ldap_username'] = _auth.get('ldap_username', ' ')
            _ret['ldap_base'] = _auth.get('ldap_base', ' ')

        return(_ret)

    def _sraps_get_transport(self, transport):
        """
            Lookup the transport (type=transport) with the providede name in the backend configuration

            We have different configuration categories, one of them is transport.
            Each endpoint has exactly one transport configured which describs through which interface (IP)
            a request to the endpoint can be made.

            Transports are also the ones which are binding to an IP and to a port using one defined protocol.

            These information is provided to the phone where the phone has to register.

            These infomration can be provided to Asterisk on different ways. The general used way is to provide
            the "bind=" configuration, which contains the IP Address, or the IP Address and the Port delimited
            with a ":". If port is not provided Asterisk and we are using 5060 as default.
            This is also true for PostgreSQL since the data structure should follow thatone.

            In case, when the interface is configured for an internal network, which is obviously directly
            not addressable from the Network (Internet, for instance), where the telephone is connecting from
            PJSIP can override this address pair using the external_signaling_address and external_signaling_port.

            Later this addresspair is used for SIP Signaling and for RTP and this is also the address, where the
            telephones have register.

            For this address resolution an extra settings under the /brige/override pass is created.
            The registrarHost and registerPort are used to override the usage of the IP Address:Port pair in such case
            when the telephone has to access to the Asterisk using a domain name (instead of the IP).
            This is useful if the telephones have to start NAPTR or SRV lookup. This is done, when the
            registerPort is left empty. In this case only the registrarHost is taken. If that is set to a domain
            the telephones will initiate a lookup an address for NAPTR, SRV and at the end A.

            Teh return of this function is a dict with the required information, however None is also possible.
            None happens, if the endpoint has a configured auth which does not exists.
            This is treated as a configuration issue.

             @param transport: the name of the requested transport

            @return: dict
                    - address: the IP Address where the transport is bound
                    - port: the portnumber
                    - protocol: it can be udp, tcp or tls.

        """

        self.log.debug("Lookup Transport for " + transport)

        _transport = self.config.get('/pjsip/transport/' + transport, None)
        if _transport is None:
            return None

        _bind = _transport['bind'].split(':')
        if len(_bind) == 1:
            if _transport['protocol'] == "tls":
                _bind.append(5061)
            else:
                _bind.append(5060)

        _r = {
            "address": (_transport['external_signaling_address']
                        if 'external_signaling_address' in _transport.keys() else _bind[0]),
            "port": (_transport['external_signaling_port']
                     if 'external_signaling_port' in _transport.keys() else _bind[1]),
            "proto": _transport['protocol'],
        }

        _override = self.config.get('/bridge/override/registrarHost', None)
        if _override:
            _r['address'] = _override
            _r['port'] = self.config.get('/bridge/override/registrarPort', '')

        _r['remote'] = _r['address']
        if _r['port'] != '':
            _r['remote'] += ':' + str(_r['port'])
        return _r

    def _sraps_update(self, _):
        """
            Create setting in SRAPS for an endpoint.

            This function creates the setting_manager, see:
            https://sraps.snom.com/documentation#tag/Endpoint/paths/~1companies~1{company_id}~1endpoints~1{mac}/put

            This object contains anothe robjects of settings. Each setting is identified by its UUID.
            Important to know, that name of some setting are overlapping between that Snom D-Series and M-Series.
            Therefore it is crucial to know what type of product is beeing configured. This is done with the get_product
            object of SRAPS object. (see sraps_api.py).

            This function combines the "static" settings and the "dynamic" settings. The static settings are taken
            from the configuration file (phone_common container) and the dynamic settings are generated here.

            These are then added to the "prov" dict.
            The prov dict has to follow the SRAPS documentation, where
                - mac: is lower case and the mac address of the telephone
                - autoprovisioning_enabled: must be set to true
                - vpn: needs to be set and here defaults to False; This option would instuct SRAPS to install the VPN
                       miniupdate to the phone.
                - setting_manager: dict, which contains every settings

            Indexing:
                Snom supports multiple identities, depending on the modell it can be 12 or more.
                This settings -as of today- is coming from the configuration object (path: /bridge/override/identity)
                and defaults to 1.
                This is however will be used for D-Series and M-Series. For D-Series it will count from 1 until 12
                and the SIP Endpoints are going to be assigned to the index from the top (from the beginning) of the
                configuration increased by one. Which means, every time, when the _SAME_ MAC address is configured for
                an extension this index increases.
                For M-Series the logic will be the same, but there a cache needs to be implemented, to ensure
                that the same extension is always provisioned to the same Handset / Slot within M-Series.
                This is still to be implemented.

            Collecting settings:
                Settings are loaded from the phone_common container of the configuration and the names
                are qualified with get_settings_by_name (see sraps_api.py). This function will return
                with a dict, key is the UUID of the setting and the settings related configuration.

            Once settings are collected the setting_manager is ready to be sent to SRAPS;
            this is done by set_endpoint of SRAPS (see: sraps_api.py).
            Based on the success of the request (the return value of the function) either a log is written,
            or, in case of a successfull update or create a Check-Sync is issued to the telephone
            to trigger the config update.

            @return: None

        """

        self.log.info("Checking Endpoints against SRAPS")
        sraps_config = {}

        for _phone in self.config.get('/pjsip/phoneprovr', {}):

            # these are phones; check if they are in sraps; if not add

            if 'mac' in _phone.keys() and _phone['mac'].startswith('000413'):

                _mac = _phone['mac']

                _product = self.sraps.get_product(_mac)

                if _product is None:
                    continue

                if _mac not in sraps_config:
                    sraps_config[_mac] = {}
                    sraps_config[_mac]['settings_manager'] = {}

                _endpoint = self.config.get('/pjsip/endpoint/' + _phone['endpoint'], None)
                if _endpoint is None:
                    continue

                _auth = self._sraps_get_auth(_endpoint['auth'])
                _transport = self._sraps_get_transport(_endpoint['transport'])
                _idx = _phone.get('idx', 1)
                _buttons = self.config.get('/pjsip/buttons/' + _mac, None)

                sraps_config[_mac]['mac'] = _mac.lower()
                sraps_config[_mac]['autoprovisioning_enabled'] = True
                sraps_config[_mac]['vpn'] = False

                # adding type specific settings
                for setting in self.config.get('/bridge/phone_common/' + _product.get('type', '') , []):
                    _value = self.config.get('/bridge/phone_common/' + _product.get('type', '') + '/' + setting, '')

                    self.log.debug2(_mac, " - adding common settings: ", setting, ' -> ', _value)

                    _uuid, _set = self.sraps.get_settings_by_name(_product, setting, _value)
                    if _set is None:
                        self.log.debug2(_mac, " - settings: ", setting, ' is invalid for the product')
                        continue

                    try:
                        sraps_config[_mac]['settings_manager'][_uuid].update(_set[_uuid])
                    except KeyError:
                        sraps_config[_mac]['settings_manager'].update(_set)

                if _product['type'] == 'MSCSeries':
                    _dynamicSettings = {
                        'sip_account_enable': {'value': 1, 'idx': _idx},
                        'authentication_access_password' : {'value': _auth['password'], 'idx': _idx},
                        'authentication_name' : {'value': _auth.get('username', ''), 'idx': _idx},
                        'user_id' : {'value': _endpoint.get('user_name', _phone['endpoint']), 'idx': _idx},
                        'display_name': {'value': _endpoint.get('display_name', _phone['endpoint']), 'idx': _idx}
                    }
                else:
                    _dynamicSettings = {
                        'user_active': {'value': True, 'idx': _idx},
                        'user_pass' : {'value': _auth['password'], 'idx': _idx},
                        'user_pname' : {'value': _auth.get('username', ''), 'idx': _idx},
                        'user_name' : {'value': _endpoint.get('user_name', _phone['endpoint']), 'idx': _idx},
                        'user_realname': {'value': _endpoint.get('display_name', _phone['endpoint']), 'idx': _idx},
                    }
                    if _auth.get('ldap_password', None) is not None:
                        _dynamicSettings['ldap_password'] = {'value': _auth['ldap_password']}
                        _dynamicSettings['ldap_username'] = {'value': _auth['ldap_username']}
                        _dynamicSettings['ldap_base'] = {'value': _auth['ldap_base']}

                for _set, _value in _dynamicSettings.items():
                    _uuid, _s = self.sraps.get_settings_by_name(_product, _set, _value)
                    if _uuid is None:
                        continue

                    try:
                        sraps_config[_mac]['settings_manager'][_uuid].update(_s[_uuid])
                    except KeyError:
                        sraps_config[_mac]['settings_manager'].update(_s)

                # Exceptions for the different series
                if _product['type'] == 'MSeries':
                    _uuid, _s = self.sraps.get_settings_by_name(
                        _product, 'user_host', {'value': _transport['remote'], 'idx': 1})
                    try:
                        sraps_config[_mac]['settings_manager'][_uuid].update(_s[_uuid])
                    except KeyError:
                        sraps_config[_mac]['settings_manager'].update(_s)

                    # subscr_sip_ua_data_server_id is the SIP Server; we only support one at the moment
                    _uuid, _s = self.sraps.get_settings_by_name(
                        _product, 'subscr_sip_ua_data_server_id', {'value': 1, 'idx': _idx})
                    try:
                        sraps_config[_mac]['settings_manager'][_uuid].update(_s[_uuid])
                    except KeyError:
                        sraps_config[_mac]['settings_manager'].update(_s)

                    _uuid, _s = self.sraps.get_settings_by_name(
                        _product, 'subscr_dect_ipui', {'value': _phone.get('ipei', '0xFFFFFFFFFF'), 'idx': _idx})
                    try:
                        sraps_config[_mac]['settings_manager'][_uuid].update(_s[_uuid])
                    except KeyError:
                        sraps_config[_mac]['settings_manager'].update(_s)

                elif _product['type'] == 'DSeries':
                    _uuid, _s = self.sraps.get_settings_by_name(
                        _product, 'user_host', {'value': _transport['remote'], 'idx': _idx})
                    try:
                        sraps_config[_mac]['settings_manager'][_uuid].update(_s[_uuid])
                    except KeyError:
                        sraps_config[_mac]['settings_manager'].update(_s)

                    # adding buttons to fkeys
                    if (_buttons is not None):
                        for _index, _fkey in _buttons.items():
                            _uuid, _s = self.sraps.get_settings_by_name(
                                _product, 'fkey',
                                {
                                    'value': _fkey.get('attr', 'none'),
                                    'idx': _index
                                },
                                type=_fkey.get('type', 'none'),
                                context=_fkey.get('context', 1),
                                label=_fkey.get('label', '!!$(::)!!$(generate_via_conditional_label_short)'),
                                short_label=_fkey.get('label', '!!$(::)!!$(generate_via_conditional_label_short)'),
                            )

                            try:
                                sraps_config[_mac]['settings_manager'][_uuid].update(_s[_uuid])
                            except KeyError:
                                sraps_config[_mac]['settings_manager'].update(_s)

                elif _product['type'] == 'MSCSeries':
                    _uuid, _s = self.sraps.get_settings_by_name(
                        _product, 'primary_sip_server_address', {'value': _transport['remote'], 'idx': _idx})
                    try:
                        sraps_config[_mac]['settings_manager'][_uuid].update(_s[_uuid])
                    except KeyError:
                        sraps_config[_mac]['settings_manager'].update(_s)

        # self.config.add('/test', sraps_config)
        # self.config.dump()

        for mac in sraps_config:
            self.log.debug2('SRAPS settings for MAC ' + mac, " ", sraps_config.get(mac))
            self.log.info("Update endpoint " + mac)

            if not self.sraps.set_endpoint(mac, sraps_config[mac]):
                self.log.error(mac, " - Can not update endpoint")
            else:
                self.log.info(mac, " - Update successful")
                # self.log.info("Updated, sending Check-Sync to ", _ep)
                # self._check_sync(_ep)

        self.log.info("SRAPS should be up-to-date")

    def _check_sync(self, endpoint='', reboot=False):
        """
            Instructs PJSIP to send a Check-Sync to the provided endpoint; this is only supported by AMI Backend

            Check-Sync is used to trigger events on the phone; Snom Phones generally downloads the configuration
            which is configured in the settin_server parameter and download the predefined confguration files, such as
            the firmware provisioning XML.
            Thhis event looks like in SIP NOTIFY:
                Event: Check-Sync
                Event: Check-Sync,reboot=false
                Event: Check-Sync,reboot=true

            Snom phones defaults not to reboot, so if the reboot option is missing the telephone will only
            load the configuration.
            However; if a new firmware is provided and update_policy
            (see: https://service.snom.com/display/wiki/update_policy) is set to "auto_update" the telephone
            will load the image and reboot.

            @param endpoint: Name of the endpoint

            @param reboot: indictaes if the endpoint is supposed to be rebooted, default to false

            @return: None

        """

        self.backend.command({
            "Action": 'PJSIPNotify',
            "ActionID": 'SIPNotify/' + endpoint,
            "Variable": "Event=check-sync;reboot=" + str(reboot),
            "Endpoint": endpoint
        })

    def set_interval(self, timer, task=None):
        """
            Internal Scheduler

            This function uses the Timer of threading to execute the given function.
            These timers are stored in the _scheduler so they can be started and cancelled individually.

            The timer parameter 0 will cancel the task, however this will not remove it from the queue.
            Using None for the task variable will cancel all tasks. This is useful before exit since a
            running thread can prevent the exit function and an exception is generated.

            @param timer: Number; represents the interval when the task is executed

            @param task: method to be executed;

            @return: None

        """
        if task is None:
            for _, job in self._scheduler.items():
                job.cancel()
            return

        isStop = task()
        if not isStop and timer > 0:
            self._scheduler[task.__name__] = Timer(timer, self.set_interval, [timer, task])
            self._scheduler[task.__name__].start()
        elif timer == 0:
            self._scheduler[task.__name__].cancel()

    def _backend_get_config(self):
        self.log.info("Reloading configuration")
        self.backend.get_config()

    def _signal_handler(self, signum, _):
        """
            Handling incoming signals

            Signals are used to instruct the process to perform an action.
            These actions are defined here.

            TERM, QUIT and INT tear down the application by stopping all scheduled tasks (see set_interval)
            and send Action: Logoff to asterisk. This is a graceful stop.

            Since we use asyncio with callbacks we expect rapid answer from Asterisk. This is needed
            if we run in a Docker containert since Docker with a STOP action will send SIGTERM to the
            PID 1 (within the container) which is expected to exit within 10 seconds.
            One this 10 seconds is over and the process was not exiting Docker will release the resources
            of the container by killing it.

            This Exit strategy is also used for the Ctrl-C (SIGINT) if the script might run in the foregorund.

            On Linux it is usual, that a process rereads the configuration upon receiving a HANGUP signal; this
            triggers the load of the configuration file using the load method. Since this is in the root of the
            configuration every other settings are also removed. Once load is done the settings of backend are
            also loaded (backend.get_config). This triggers the same bootstrap scenario,
            like the skript was just started.

            We implement also two USER Signals; the first is to send Check-Sync event to every registered endpoints.
            This is done by requesting a list of local registered endpoints (PJSIPShowRegistrationsInbound) and
            handle the return with the CheckSyncRequest handler.

            @param signum: the number of the signal

            @return: None

        """

        self.log.debug("Signal received", signum)

        if signum in (signal.SIGTERM, signal.SIGQUIT, signal.SIGINT):
            self.log.debug("Exit signal received")
            self.set_interval(0)

            self.backend.command({
                "Action": 'Logoff',
                "ActionID": 'Logoff'
            })

        elif signum == signal.SIGHUP:
            self.config.load()
            self.backend.get_config()

        elif signum == signal.SIGUSR1:
            self.log.debug("Prepare sending Check-Sync to every registered endpoints")

            self.backend.command({
                "Action": 'PJSIPShowRegistrationsInbound',
                "ActionID": 'CheckSyncRequest'
            })

        elif signum == signal.SIGUSR2:
            self.config.dump()

        else:
            self.log.info("Unhandled Signal.")

if __name__ == '__main__':

    # define main structure
    MAIN = Main()
    MAIN.start()
